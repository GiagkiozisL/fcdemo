package io.fieldcode.conference.data.local

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import io.fieldcode.conference.model.Address
import io.fieldcode.conference.model.Company
import io.fieldcode.conference.model.Geo
import io.fieldcode.conference.model.User
import junit.framework.TestCase
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import java.io.IOException

class AppDatabaseTest : TestCase() {
    private lateinit var userDao: UserDao
    private lateinit var db: AppDatabase

    @Before
    public override fun setUp() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            context, AppDatabase::class.java
        ).build()
        userDao = db.userDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    fun writeAndReadSpend() = runBlocking {
        val geo = Geo(0.1, 0.2)
        val address = Address("astreet", "asuite", "acity", "azzipcode", geo)
        val company = Company("aname", "acatchPhrase", "abs")
        val user = User(0, "aname", "ausername", "aemail", address, "aphone", "awebsite", company)
        userDao.insertUser(user)
        val users = userDao.getLast20Users()
        assert(users?.contains(user) == true)
    }
}