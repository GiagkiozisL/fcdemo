package io.fieldcode.conference.model

data class Geo(
    val lat: Double,
    val lng: Double
) {
}