package io.fieldcode.conference.util

class GeneralUtils {

    companion object {
        const val STRING_LENGTH = 10;
        const val ALPHANUMERIC_REGEX = "[a-zA-Z0-9]+";

        fun randomAlphanumeric(length: Int): String {
            val allowedChars = ('A'..'Z') + ('a'..'z') + ('0'..'9')
            return (1..length)
                .map { allowedChars.random() }
                .joinToString("")
        }
    }
}