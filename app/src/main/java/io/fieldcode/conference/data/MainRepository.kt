package io.fieldcode.conference.data

import io.fieldcode.conference.data.local.UserDao
import io.fieldcode.conference.data.remote.UserRemoteDataSource
import io.fieldcode.conference.model.Post
import io.fieldcode.conference.model.Result
import io.fieldcode.conference.model.User
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class MainRepository @Inject constructor(private val userRemoteDataSource: UserRemoteDataSource,
                                         private val userDao: UserDao
) {

    suspend fun getUsers(): Flow<Result<List<User>>?> {
        return flow {
            emit(fetchUsersCached())
            emit(Result.loading())

            val result = userRemoteDataSource.fetchUsers()

            //Cache to database if response is successful
            if (result.status == Result.Status.SUCCESS) {
                result.data?.let { it ->
                    userDao.deleteAllUsers(it)
                    userDao.insertBatchUsers(it)
                }
            }
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getPosts(): Flow<Result<List<Post>>?> {
        return flow {
            emit(fetchPosts())
            emit(Result.loading())

            val result = userRemoteDataSource.fetchPosts()

            //Cache to database if response is successful
            if (result.status == Result.Status.SUCCESS) {
                result.data?.let { it ->
                    userDao.deleteAllPosts(it)
                    userDao.updateBatchPosts(it)
                }
            }
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getPostsById(userId: Int): Flow<Result<List<Post>>?> {
        return flow {
            emit(fetchPostsByUserId(userId))
            emit(Result.loading())

            val result = userRemoteDataSource.fetchPostsById(userId)

            //Cache to database if response is successful
            if (result.status == Result.Status.SUCCESS) {
                result.data?.let { it ->
                    userDao.deleteAllPosts(it)
                    userDao.updateBatchPosts(it)
                }
            }
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    suspend fun createPost(userId: Int, title: String, body: String): Flow<Result<Post>> {
        return flow {
            emit(Result.loading())

            val result = userRemoteDataSource.createPost(userId, title, body)

            if (result.status == Result.Status.SUCCESS) {
                result.data?.let { it ->
                    userDao.insertPost(it)
                }
            }
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    private fun fetchUsersCached(): Result<List<User>> =
        userDao.queryUsers().let {
            Result.success(it)
        }

    private fun fetchPosts(): Result<List<Post>> =
        userDao.queryPosts().let {
            Result.success(it)
        }

    private fun fetchPostsByUserId(userId: Int): Result<List<Post>> =
        userDao.queryPostsById(userId).let {
            Result.success(it)
        }
}
