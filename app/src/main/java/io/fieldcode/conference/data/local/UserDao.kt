package io.fieldcode.conference.data.local

import androidx.room.*
import io.fieldcode.conference.model.Post
import io.fieldcode.conference.model.User

@Dao
interface UserDao {

    // region //// user table operations ////
    @Query("SELECT * FROM users ORDER BY id DESC LIMIT 20")
    suspend fun getLast20Users(): List<User>?

    @Query("SELECT * FROM users ORDER BY id")
    fun queryUsers(): List<User>?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(user: User)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertBatchUsers(users: List<User>)

    @Delete
    fun deleteAllUsers(users: List<User>)
    // endregion //// user table operations ////

    // region //// post table operations ////
    @Query("SELECT * FROM posts WHERE userId == :uId")
    fun queryPostsById(uId: Int): List<Post>?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPost(post: Post)

    @Query("SELECT * FROM posts")
    fun queryPosts(): List<Post>?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun updateBatchPosts(posts: List<Post>)

    @Delete
    fun deleteAllPosts(posts: List<Post>)
    // endregion //// post table operations ////
}