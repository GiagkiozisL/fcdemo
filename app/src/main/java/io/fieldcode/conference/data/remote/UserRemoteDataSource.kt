package io.fieldcode.conference.data.remote

import io.fieldcode.conference.model.Post
import io.fieldcode.conference.model.Result
import io.fieldcode.conference.model.User
import io.fieldcode.conference.util.ErrorUtils
import retrofit2.Response
import retrofit2.Retrofit
import javax.inject.Inject

class UserRemoteDataSource @Inject constructor(private val retrofit: Retrofit) {

    suspend fun fetchUsers(): Result<List<User>> {
        val userService = retrofit.create(ApiInterface::class.java);
        return getResponse(
            request = { userService.getUsers() },
            defaultErrorMessage = "Error fetching user list")
    }

    suspend fun fetchPosts(): Result<List<Post>> {
        val userService = retrofit.create(ApiInterface::class.java);
        return getResponse(
            request = { userService.getPosts() },
            defaultErrorMessage = "Error fetching user list")
    }

    suspend fun fetchPostsById(userId: Int): Result<List<Post>> {
        val userService = retrofit.create(ApiInterface::class.java);
        return getResponse(
            request = { userService.getPostsById(userId) },
            defaultErrorMessage = "Error fetching user list")
    }

    suspend fun createPost(userId: Int, title: String, body: String): Result<Post> {
        val userService = retrofit.create(ApiInterface::class.java);
        return getResponse(
            request = { userService.createPost(Post(userId, title, body)) },
            defaultErrorMessage = "Error fetching user list")
    }

    private suspend fun <T> getResponse(request: suspend () -> Response<T>, defaultErrorMessage: String): Result<T> {
        return try {
            println("I'm working in thread ${Thread.currentThread().name}")
            val result = request.invoke()
            if (result.isSuccessful) {
                return Result.success(result.body())
            } else {
                val errorResponse = ErrorUtils.parseError(result, retrofit)
                Result.error(errorResponse?.status_message ?: defaultErrorMessage, errorResponse)
            }
        } catch (e: Throwable) {
            Result.error("Error: " + e.message, null)
        }
    }
}