package io.fieldcode.conference.data.remote

import io.fieldcode.conference.model.Post
import io.fieldcode.conference.model.User
import retrofit2.Response
import retrofit2.http.*

interface ApiInterface {

    @GET("/users")
    suspend fun getUsers(): Response<List<User>>

    //https://jsonplaceholder.typicode.com/posts?userId=1
    @GET("/posts")
    suspend fun getPostsById(@Query("userId") userId: Int): Response<List<Post>>

    //https://jsonplaceholder.typicode.com/posts
    @GET("/posts")
    suspend fun getPosts(): Response<List<Post>>

    @Headers("Content-Type: application/json")
    @POST("/posts")
    suspend fun createPost(@Body post: Post): Response<Post>

}