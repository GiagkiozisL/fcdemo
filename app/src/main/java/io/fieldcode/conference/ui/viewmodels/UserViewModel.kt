package io.fieldcode.conference.ui.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import io.fieldcode.conference.data.MainRepository
import io.fieldcode.conference.model.Post
import io.fieldcode.conference.model.Result
import io.fieldcode.conference.model.User
import io.fieldcode.conference.util.SingleLiveEvent
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class UserViewModel @Inject constructor(
    private val mainRepository: MainRepository) : ViewModel() {

    private val _userList = MutableLiveData<Result<List<User>>>()
    private val _postList = MutableLiveData<Result<List<Post>>>()
    val navigateToPost = SingleLiveEvent<Int>()
    val navigateToUrl = SingleLiveEvent<String>()
    val userList = _userList
    val postList = _postList

    init {
        fetchUsers()
        fetchPosts()
    }

    private fun fetchUsers() {
        viewModelScope.launch {
            mainRepository.getUsers().collect {
                _userList.value = it
            }
        }
    }

    private fun fetchPosts() {
        viewModelScope.launch {
            mainRepository.getPosts().collect {
                _postList.value = it
            }
        }
    }

    fun userClicked(userId: Int) {
        navigateToPost.value = userId
    }

    fun websiteClicked(url: String) {
        navigateToUrl.value = url
    }
}