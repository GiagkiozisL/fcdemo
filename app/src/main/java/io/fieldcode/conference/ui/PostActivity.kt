package io.fieldcode.conference.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import io.fieldcode.conference.R
import io.fieldcode.conference.databinding.ActivityPostBinding
import io.fieldcode.conference.databinding.BottomSheetDialogBinding
import io.fieldcode.conference.model.Result
import io.fieldcode.conference.ui.adapter.PostsAdapter
import io.fieldcode.conference.ui.viewmodels.PostViewModel
import io.fieldcode.conference.util.GeneralUtils.Companion.randomAlphanumeric
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

const val EXTRA_USER_ID = "EXTRA_USER_ID"

@AndroidEntryPoint
class PostActivity : AppCompatActivity() {

    private lateinit var binding: ActivityPostBinding
    private val viewModel by viewModels<PostViewModel>()
    private lateinit var postsAdapter: PostsAdapter

    private var userId: Int = 1
    companion object {
        // Whenever we want to create this Activity, we use it via this intent creation function.
        fun getStartIntent(context: Context, userId: Int): Intent {
            return Intent(context, PostActivity::class.java)
                .putExtra(EXTRA_USER_ID, userId)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityPostBinding.inflate(layoutInflater)
        setContentView(binding.root)

        userId = intent.getIntExtra(EXTRA_USER_ID, 1)

        initRecyclerView()
        observePosts()

        binding.postCreationBtn.setOnClickListener {
            openBottomSheet()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_post, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_asc -> {
                // sort particular list asc
                viewModel.sort(false)
                true
            }
            R.id.action_desc -> {
                // sort particular list desc
                viewModel.sort(true)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun initRecyclerView() {
        val gridLayoutManager = GridLayoutManager(this, 2)
        gridLayoutManager.orientation = GridLayoutManager.VERTICAL
        postsAdapter = PostsAdapter(ArrayList())
        binding.postRecyclerView.adapter = postsAdapter;
        binding.postRecyclerView.apply {
            setHasFixedSize(true)
            layoutManager = gridLayoutManager
        }
    }

    private fun observePosts() {
        viewModel.postList.observe(this, Observer { result ->
            when (result.status) {
                Result.Status.SUCCESS -> {
                    result.data?.let { list ->
                        postsAdapter.updateData(list)
                    }
                }

                Result.Status.ERROR -> {
                    result.message?.let {
                        showError(it)
                    }
                }

                Result.Status.LOADING -> {

                }
            }
        })
        viewModel.fetchPosts(userId)
    }

    @ExperimentalCoroutinesApi
    private fun observePostCreation() {
        viewModel.aPost.observe(this, Observer { result ->
            when (result.status) {
                Result.Status.SUCCESS -> {
                    result.data?.let { post ->
                        postsAdapter.addPost(post)
                        binding.postRecyclerView.scrollToPosition(postsAdapter.itemCount - 1)
                    }
                }

                Result.Status.ERROR -> {
                    result.message?.let {
                        showError(it)
                    }
                }

                Result.Status.LOADING -> { }
            }
        })
        viewModel.createPost(userId)
    }

    private fun showError(msg: String) {
        Snackbar.make(binding.postParent, msg, Snackbar.LENGTH_INDEFINITE).setAction("DISMISS") {
        }.show()
    }

    @ExperimentalCoroutinesApi
    private fun openBottomSheet() {

        val dialog = BottomSheetDialog(this)

        val bottomSheetDialogBinding = BottomSheetDialogBinding.inflate(layoutInflater)
        bottomSheetDialogBinding.bottomSheetTitleTxt.text = getString(R.string.user_post_creation).format(userId)

        bottomSheetDialogBinding.bottomSheetSendBtn.setOnClickListener {
            observePostCreation()
            dialog.dismiss()
        }

        bottomSheetDialogBinding.bottomSheetTitleEditText.watchText {
            viewModel.setTitle(it.toString())
        }
        bottomSheetDialogBinding.bottomSheetBodyEditText.watchText {
            viewModel.setBody(it.toString())
        }

        // im generating random title/body for test
        val randomTitle = randomAlphanumeric(10)
        val randomBody = randomAlphanumeric(40)
        viewModel.setTitle(randomTitle)
        viewModel.setBody(randomBody)
        bottomSheetDialogBinding.bottomSheetTitleEditText.setText(randomTitle)
        bottomSheetDialogBinding.bottomSheetBodyEditText.setText(randomBody)

        lifecycleScope.launch {
            viewModel.isSubmitEnabled.collect { value ->
                bottomSheetDialogBinding.bottomSheetSendBtn.isEnabled = value
            }
        }

        dialog.setContentView(bottomSheetDialogBinding.root) //view)
        dialog.show()
    }

    fun EditText.watchText(afterChanged: (text: String?) -> Unit) {
        addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(editable: Editable) {

                afterChanged(editable.toString())
            }
        })
    }
}