package io.fieldcode.conference.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageButton
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import io.fieldcode.conference.R
import io.fieldcode.conference.model.Address
import io.fieldcode.conference.model.Company
import io.fieldcode.conference.model.User
import io.fieldcode.conference.ui.MainActivity
import java.util.*

class UsersAdapter (users: List<User>?,
                    private val onUserClicked: (userId: Int) -> Unit,
                    private val onWebsiteClicked: (url: String) -> Unit,)
    : RecyclerView.Adapter<UsersAdapter.UsersViewHolder>() {

    private var userList = ArrayList<User>()

    init {
        this.userList = users as ArrayList<User>
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UsersViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.user_list_item, parent, false)
        return UsersViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return userList.size
    }

    override fun onBindViewHolder(holder: UsersViewHolder, position: Int) {
        val cryptocurrencyItem = userList[position]
        holder.userListItem(cryptocurrencyItem)

        holder.postsBtn.setOnClickListener {
            if (holder.adapterPosition != RecyclerView.NO_POSITION) {
                onUserClicked.invoke(userList[position].id)
            }
        }

        holder.webBtn.setOnClickListener {
            if (holder.adapterPosition != RecyclerView.NO_POSITION) {
                onWebsiteClicked.invoke(userList[position].website)
            }
        }
    }

    fun addUsers(users: List<User>){
        val initPosition = userList.size
        userList.addAll(users)
        notifyItemRangeInserted(initPosition, userList.size)
    }

    fun updateData(newList: List<User>) {
        userList.clear()
        val sortedList = newList.sortedBy { user -> user.id }
        userList.addAll(sortedList)
        notifyDataSetChanged()
    }

    class UsersViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var userName = itemView.findViewById<AppCompatTextView>(R.id.user_item_name)
        var userAlias = itemView.findViewById<AppCompatTextView>(R.id.user_item_user_name)
        var userAddress = itemView.findViewById<AppCompatTextView>(R.id.user_item_address)
        var userCompany = itemView.findViewById<AppCompatTextView>(R.id.user_item_company)
        var mailBtn = itemView.findViewById<AppCompatImageButton>(R.id.user_item_mail_btn)
        var webBtn = itemView.findViewById<AppCompatImageButton>(R.id.user_item_web_btn)
        var phoneBtn = itemView.findViewById<AppCompatImageButton>(R.id.user_item_phone_btn)
        var postsBtn = itemView.findViewById<AppCompatImageButton>(R.id.user_item_post_btn)

        fun userListItem(userItem: User) {
            userName.text = userItem.name
            userAlias.text = userItem.username
            userAddress.text = concatAddress(userItem)
            userCompany.text = concatCompany(userItem)
        }

        private fun concatCompany(user: User) : String {
            val company: Company = user.company
            return "${company.name} (${company.bs})"
        }

        private fun concatAddress(user: User) : String {
            val address: Address = user.address
            return "${address.street} ${address.suite}, ${address.city}"
        }
    }
}
