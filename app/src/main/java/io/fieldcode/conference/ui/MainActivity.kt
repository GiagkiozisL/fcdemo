package io.fieldcode.conference.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.webkit.URLUtil
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import io.fieldcode.conference.databinding.ActivityMainBinding
import io.fieldcode.conference.model.Result
import io.fieldcode.conference.ui.adapter.UsersAdapter
import io.fieldcode.conference.ui.viewmodels.UserViewModel


@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val viewModel by viewModels<UserViewModel>()
    private lateinit var usersAdapter: UsersAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initRecyclerView()
        subscribeUi()
    }

    private fun initRecyclerView() {
        val linearLayoutManager = LinearLayoutManager(this)
        val onUserClicked: (userId: Int) -> Unit = { userId ->
            viewModel.userClicked(userId)
        }

        val onWebsiteClicked: (url: String) -> Unit = { url ->
            viewModel.websiteClicked(url)
        }
        usersAdapter = UsersAdapter(ArrayList(), onUserClicked, onWebsiteClicked)
        binding.mainRecyclerView.adapter = usersAdapter
        binding.mainRecyclerView.apply {
            setHasFixedSize(true)
            layoutManager = linearLayoutManager
        }
    }

    private fun subscribeUi() {
        observeUsers()
        observePosts()
        observeUserSelected()
        observeWebsiteClicked()
    }

    private fun openUrl(url: String) {
        if (!URLUtil.isValidUrl(url)) {
            Toast.makeText(this, "Invalid url", Toast.LENGTH_LONG).show()
            return
        }

        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(url)
        startActivity(intent)
    }

    // region //// observers ////
    private fun observeUsers() {
        viewModel.userList.observe(this, Observer { result ->
            when (result.status) {
                Result.Status.SUCCESS -> {
                    result.data?.let { list ->
                        usersAdapter.updateData(list)
                    }
                    binding.mainProgressBar.visibility = View.GONE
                }

                Result.Status.ERROR -> {
                    result.message?.let {
                        showError(it)
                    }
                    binding.mainProgressBar.visibility = View.GONE
                }

                Result.Status.LOADING -> {
                    binding.mainProgressBar.visibility = View.VISIBLE
                }
            }
        })
    }

    private fun observePosts() {
        viewModel.postList.observe(this, Observer { result ->
            when (result.status) {
                Result.Status.SUCCESS -> {
                    result.data?.let { list ->
//                        usersAdapter.updateData(list)
                    }
//                    binding.mainProgressBar.visibility = View.GONE
                }

                Result.Status.ERROR -> {
                    result.message?.let {
                        showError(it)
                    }
//                    binding.mainProgressBar.visibility = View.GONE
                }

                Result.Status.LOADING -> {
//                    binding.mainProgressBar.visibility = View.VISIBLE
                }
            }
        })
    }

    private fun observeUserSelected() {
        viewModel.navigateToPost.observe(this, Observer { userId ->
            if (userId != null) startActivity(PostActivity.getStartIntent(this, userId))
        })
    }

    private fun observeWebsiteClicked() {
        viewModel.navigateToUrl.observe(this, Observer { url ->
            openUrl(url)
        })
    }

    private fun showError(msg: String) {
        Snackbar.make(binding.vParent, msg, Snackbar.LENGTH_INDEFINITE).setAction("DISMISS") {
        }.show()
    }
    // endregion //// observers ////
}