package io.fieldcode.conference.ui.viewmodels

import androidx.lifecycle.*
import dagger.hilt.android.lifecycle.HiltViewModel
import io.fieldcode.conference.data.MainRepository
import io.fieldcode.conference.model.Post
import io.fieldcode.conference.model.Result
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class PostViewModel @Inject constructor(
    private val mainRepository: MainRepository) : ViewModel() {

    private val _postList = MutableLiveData<Result<List<Post>>>()
    private val _aPost = MutableLiveData<Result<Post>>()
    val postList = _postList
    val aPost = _aPost

    @ExperimentalCoroutinesApi
    private val _title = MutableStateFlow("")
    @ExperimentalCoroutinesApi
    private val _body = MutableStateFlow("")

    init { }

    @ExperimentalCoroutinesApi
    fun setTitle(title: String) {
        _title.value = title
    }

    @ExperimentalCoroutinesApi
    fun setBody(body: String) {
        _body.value = body
    }

    @ExperimentalCoroutinesApi
    val isSubmitEnabled: Flow<Boolean> = combine(_title, _body) { _, _ ->
        val isTitleCorrect = !_title.value.isEmpty()
        val isBodyCorrect = !_body.value.isEmpty()
        return@combine isTitleCorrect and isBodyCorrect
    }

    fun fetchPosts(userid: Int) {
        viewModelScope.launch {
            mainRepository.getPostsById(userid).collect {
                _postList.value = it
            }
        }
    }

    @ExperimentalCoroutinesApi
    fun createPost(userId: Int) {
        viewModelScope.launch {
            mainRepository.createPost(userId, _title.value, _body.value).collect {
                _aPost.value = it
            }
        }
    }

    fun sort(isDesc: Boolean) {
        val sortedList: List<Post>?
        sortedList = if (isDesc) {
            _postList.value?.data?.sortedByDescending { post -> post.id }
        } else {
            _postList.value?.data?.sortedBy { post -> post.id }
        }
        _postList.value = Result(Result.Status.SUCCESS, sortedList, null, null)
    }
}