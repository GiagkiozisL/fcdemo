package io.fieldcode.conference.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import io.fieldcode.conference.R
import io.fieldcode.conference.model.Post
import java.util.*

class PostsAdapter (users: List<Post>?)
    : RecyclerView.Adapter<PostsAdapter.PostViewHolder>() {

    private var postList = ArrayList<Post>()

    init {
        this.postList = users as ArrayList<Post>
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.post_list_item, parent, false)
        return PostViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return postList.size
    }

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        val cryptocurrencyItem = postList[position]
        holder.postListItem(cryptocurrencyItem)
    }

    fun addPost(post: Post){
        postList.add(post)
        val initPosition = postList.size
        notifyItemRangeInserted(initPosition, postList.size) // asc
    }

    fun updateData(newList: List<Post>) {
        postList.clear()
        postList.addAll(newList)
        notifyDataSetChanged()
    }

    class PostViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var title = itemView.findViewById<AppCompatTextView>(R.id.post_item_title)
        var body = itemView.findViewById<AppCompatTextView>(R.id.post_item_body)

        fun postListItem(postItem: Post) {
            title.text = postItem.title
            body.text = postItem.body
        }
    }
}